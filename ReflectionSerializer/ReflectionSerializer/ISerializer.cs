﻿namespace ReflectionSerializer
{
    public interface ISerializer
    {
        string Serialize<T>(T obj);

        T Deserialize<T>(string obj);
    }
}