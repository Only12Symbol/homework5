﻿using System.Linq;
using TestingSerializer = ReflectionSerializer.Implementation;
using Xunit;
using System.Text.Json;
using UnitTests.Helpers;

namespace UnitTests
{
    public class JsonSerializerTests
    {
        private readonly TestingSerializer.JsonSerializer _jsonSerializer;

        public JsonSerializerTests()
        {
            _jsonSerializer = new TestingSerializer.JsonSerializer();
        }

        [Fact]
        public void SerializeObject_WithTestClass_Serialized()
        {
            var value = new {i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5};
            var expected = JsonSerializer.Serialize(value);

            var actual = _jsonSerializer.Serialize(value);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DeserializeObject_WithString_Deserialized()
        {
            const string value = "{\"i1\":1,\"i2\":2,\"i3\":3,\"i4\":4,\"i5\":5}";
            var expected = JsonSerializer.Deserialize<object>(value);

            var actual = _jsonSerializer.Deserialize<object>(value);

            var anonymousEquality = AnonymousTypeHelper.Equal(expected, actual);
            Assert.True(anonymousEquality);
        }
    }
}