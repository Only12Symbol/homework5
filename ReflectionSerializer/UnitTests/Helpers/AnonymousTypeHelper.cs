﻿using System.Linq;

namespace UnitTests.Helpers
{
    public class AnonymousTypeHelper
    {
        public static bool Equal(object a, object b)
        {
            var aObjectPropertyInfos = a.GetType().GetProperties();
            var bObjectPropertyInfos = b.GetType().GetProperties();

            return !aObjectPropertyInfos
                .Except(bObjectPropertyInfos)
                .Any();
        }
    }
}